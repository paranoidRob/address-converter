package de.pernpas;

import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class AddresConverter {

	private static final String UTF8_BOM = "\uFEFF";
	private static final String REPLACEMENT_CHAR = "\uFFFD";

	private String pathToInput;

	private KundeMapper kundeMapper;
	private ExcelWrtier writer;

	public AddresConverter(String pathToInput, String pathToOutput) {
		this.pathToInput = pathToInput;
		this.kundeMapper = new KundeMapper();
		this.writer = new ExcelWrtier(pathToOutput);
	}

	public void convert() throws Exception {
		List<Kunde> kunden = new ArrayList<>();
		ArrayList<String> lines = readLines();
		for (String line : lines) {
			String l = removeTrash(line);
			if (StringUtils.isNotBlank(l)) {
				kunden.add(kundeMapper.map(l));
			}
		}
		writer.write(kunden);
	}

	private String removeTrash(String line) throws UnsupportedEncodingException {
		String result = line;
		result = StringUtils.replaceChars(result, REPLACEMENT_CHAR, "");
		result = StringUtils.replaceChars(result, UTF8_BOM, "");
		result = StringUtils.replaceChars(result, "\r", "");
		return StringUtils.trim(result);
	}

	private ArrayList<String> readLines() throws Exception {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(Paths.get(pathToInput), "UTF-16LE").useDelimiter("\n");
		ArrayList<String> lines = new ArrayList<String>();
		Integer counter = 1;
		while (scanner.hasNext()) {
			String line = scanner.next();
			line = removeTrash(line);
			if (StringUtils.isNotBlank(line) && line.startsWith(counter.toString())) {
				lines.add(line);
				counter++;
			}
		}
		scanner.close();
		return lines;
	}

}
