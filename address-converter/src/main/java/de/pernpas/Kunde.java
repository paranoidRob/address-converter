package de.pernpas;

public class Kunde {

	private int linenumber;
	private String vorname;
	private String nachname;
	private String strasse;
	private String plzOrt;

	public int getLinenumber() {
		return linenumber;
	}

	public void setLinenumber(int linenumber) {
		this.linenumber = linenumber;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getPlzOrt() {
		return plzOrt;
	}

	public void setPlzOrt(String plzOrt) {
		this.plzOrt = plzOrt;
	}

	@Override
	public String toString() {
		return "Kunde [linenumber=" + linenumber + ", vorname=" + vorname + ", nachname=" + nachname + ", strasse=" + strasse + ", plzOrt=" + plzOrt + "]";
	}

}
