package de.pernpas;

import static org.apache.commons.lang3.StringUtils.trim;

import org.apache.commons.lang3.StringUtils;

public class KundeMapper {

	public Kunde map(String l) {
		try {
			Kunde kunde = new Kunde();
			kunde.setLinenumber(Integer.parseInt(trim(l.substring(0, 3))));
			kunde.setVorname(trim(l.substring(15, 24)));
			kunde.setNachname(trim(l.substring(24, 38)));
			String strasseHausNrPlzOrt = l.substring(38, l.length());
			kunde.setStrasse(getStrasseHausnr(strasseHausNrPlzOrt));
			kunde.setPlzOrt(trim(StringUtils.substringAfter(strasseHausNrPlzOrt, "-")));
			return kunde;
		} catch (Exception e) {
			return null;
		}
	}

	private String getStrasseHausnr(String strasseHausNrPlzOrt) {
		return StringUtils.trim(StringUtils.substringBefore(strasseHausNrPlzOrt, "-"));
	}

}
