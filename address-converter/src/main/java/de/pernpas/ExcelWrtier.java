package de.pernpas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelWrtier {

	private String pathToOutput;

	public ExcelWrtier(String pathToOutput) {
		this.pathToOutput = pathToOutput;
	}

	public void write(List<Kunde> kunden) throws IOException {
		FileOutputStream out = new FileOutputStream(pathToOutput);
		Workbook wb = new HSSFWorkbook();
		Sheet s = wb.createSheet();
		wb.setSheetName(0, "Adressliste");

		for (Kunde kunde : kunden) {
			Row r = s.createRow(kunde.getLinenumber());
			Cell c1 = r.createCell(0);
			c1.setCellValue(kunde.getVorname());

			Cell c2 = r.createCell(1);
			c2.setCellValue(kunde.getNachname());

			Cell c3 = r.createCell(2);
			c3.setCellValue(kunde.getPlzOrt());

			Cell c4 = r.createCell(3);
			c4.setCellValue(kunde.getStrasse());
		}

		wb.write(out);
		out.close();
	}

}
